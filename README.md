# Extension of Patoline emacs input method

My extension of Patoline's emacs input method, including calligraphic,
bold, frak, mathbb fonts, plus lots of combining symbols, delimiters,
etc.
